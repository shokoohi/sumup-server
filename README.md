# Sumup-server

## Configuration

Create a file called `.env` placed at the root of your working directory. (this file *MUST* be gitignored)

```dotenv
# Your environments
SECRET_KEY=231dwadaw77dawknknbjbnjkbujbuj889047rsx
DATABASE_HOST=127.0.0.1
DATABASE_PORT=5432
DATABASE_NAME=postgres
DATABASE_USER=postgres
DATABASE_PASSWORD=
PGADMIN_DEFAULT_EMAIL=postgres@gmail.com
PGADMIN_DEFAULT_PASSWORD=jhbadwhaw65567dwabhjk989da233adw1840ap
```
### Export environment values
```bash
export $(grep -v '^#' .env | xargs)
````

## Run
### Manual
```bash
python3 -m venv .venv/
source .venv/bin/activate
pip install -U -r requirements.txt
python manage.py runserver 0.0.0.0:8000
```

### Docker

You can also run this project using docker/docker-compose.

An alpine image is used for the sake of simplicity.


```.bash
docker-compose up -d
```
