# Generated by Django 2.2.4 on 2019-09-04 19:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0027_auto_20190904_2347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='hash_tags',
            field=models.ManyToManyField(to='api.HashTag'),
        ),
    ]
