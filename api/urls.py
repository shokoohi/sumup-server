from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework.routers import DefaultRouter
from api.views import CountryViewSet, PhoneViewSet, GradeViewSet, MajorViewSet, OrientationViewSet, ClassViewSet, \
    UserViewSet, HashTagViewSet, MessageViewSet, ImageViewSet, VideoViewSet, DocumentViewSet, LikeViewSet, \
    DirectViewSet, NotificationViewSet

# Create a router and register our ViewSets with it.
router = DefaultRouter()
router.register(prefix='country', viewset=CountryViewSet)
router.register(prefix='phone', viewset=PhoneViewSet)
router.register(prefix='grade', viewset=GradeViewSet)
router.register(prefix='major', viewset=MajorViewSet)
router.register(prefix='orientation', viewset=OrientationViewSet)
router.register(prefix='class', viewset=ClassViewSet)
router.register(prefix='user', viewset=UserViewSet)
router.register(prefix='hash-tag', viewset=HashTagViewSet)
router.register(prefix='message', viewset=MessageViewSet)
router.register(prefix='image', viewset=ImageViewSet)
router.register(prefix='video', viewset=VideoViewSet)
router.register(prefix='document', viewset=DocumentViewSet)
router.register(prefix='like', viewset=LikeViewSet)
router.register(prefix='direct', viewset=DirectViewSet)
router.register(prefix='notification', viewset=NotificationViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('api/', include(router.urls)),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
