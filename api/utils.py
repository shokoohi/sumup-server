from uuid import uuid4


def get_new_file_name(instance, filename):
    split_file_name = filename.split(".")
    name = "-".join(split_file_name[:-1])
    ext = split_file_name[-1]
    new_file_name = f"{name}-{instance.message.id}-{uuid4()}.{ext}"
    return new_file_name


def images_directory_path(instance, filename):
    new_file_name = get_new_file_name(instance=instance, filename=filename)
    return f"uploads/images/{new_file_name}"


def videos_directory_path(instance, filename):
    new_file_name = get_new_file_name(instance=instance, filename=filename)
    return f"uploads/videos/{new_file_name}"


def documents_directory_path(instance, filename):
    new_file_name = get_new_file_name(instance=instance, filename=filename)
    return f"uploads/documents/{new_file_name}"
