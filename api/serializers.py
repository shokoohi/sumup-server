from rest_framework import serializers
from api.models import Country, Phone, Grade, Major, Orientation, Class, User, HashTag, Message, Image, Video, \
    Document, Like, Direct, Notification


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['id', 'official_name', 'short_name', 'numeric_code', 'phone_code']


class PhoneSerializer(serializers.ModelSerializer):
    country_data = CountrySerializer(source='country', read_only=True)

    class Meta:
        model = Phone
        fields = ['id', 'country', 'activated', 'number', 'country_data']


class GradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grade
        fields = ['id', 'name', 'minimum_level', 'maximum_level']


class MajorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Major
        fields = ['id', 'name']


class OrientationSerializer(serializers.ModelSerializer):
    major_data = MajorSerializer(source='major', read_only=True)

    class Meta:
        model = Orientation
        fields = ['id', 'major', 'name', 'major_data']


class ClassSerializer(serializers.ModelSerializer):
    grade_data = GradeSerializer(source='grade', read_only=True)
    major_data = MajorSerializer(source='major', read_only=True)
    orientation_data = OrientationSerializer(source='orientation', read_only=True)

    class Meta:
        model = Class
        fields = ['id', 'grade', 'major', 'orientation', 'level', 'grade_data', 'major_data', 'orientation_data']


class UserSerializer(serializers.ModelSerializer):
    country_data = CountrySerializer(source='country', read_only=True)
    phone_data = PhoneSerializer(source='phone', read_only=True)
    class_level_data = ClassSerializer(source='class_level', read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'email', 'avatar', 'country', 'phone', 'class_level',
                  'last_seen', 'country_data', 'phone_data', 'class_level_data']


class HashTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = HashTag
        fields = ['id', 'tag']


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['id', 'file', 'caption', 'message']


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ['id', 'file', 'caption', 'message']


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ['id', 'file', 'caption', 'message']


class LikeSerializer(serializers.ModelSerializer):
    user_data = UserSerializer(source='user', read_only=True)

    class Meta:
        model = Like
        fields = ['id', 'user', 'date', 'message', 'user_data']


class MessageSerializer(serializers.ModelSerializer):
    user_data = UserSerializer(source='user', read_only=True)
    hash_tags_data = HashTagSerializer(source='hash_tags', read_only=True, many=True)
    images_data = ImageSerializer(source='images', read_only=True, many=True)
    videos_data = VideoSerializer(source='videos', read_only=True, many=True)
    documents_data = DocumentSerializer(source='documents', read_only=True, many=True)
    likes_data = LikeSerializer(source='likes', read_only=True, many=True)
    comments_data = serializers.SerializerMethodField(read_only=True)
    reply_to_user_data = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Message
        fields = ['id', 'text', 'date', 'user', 'hash_tags', 'images', 'videos', 'documents', 'reply_to', 'likes',
                  'comments', 'user_data', 'hash_tags_data', 'images_data', 'videos_data', 'documents_data',
                  'reply_to_user_data', 'likes_data', 'comments_data']

    @staticmethod  # Remove this line if you want to use 'self'
    def get_comments_data(message):
        all_comments = message.comments.all()
        if all_comments.count() > 0:
            comment_serializer = MessageSerializer(instance=all_comments, many=True)
            return comment_serializer.data
        return list()

    @staticmethod  # Remove this line if you want to use 'self'
    def get_reply_to_user_data(message):
        reply_to = message.reply_to
        if reply_to is not None:
            reply_to_user_serializer = UserSerializer(instance=reply_to.user)
            return reply_to_user_serializer.data
        return None


class DirectSerializer(serializers.ModelSerializer):
    from_user_data = UserSerializer(source='from_user', read_only=True)

    class Meta:
        model = Direct
        fields = ['id', 'text', 'date', 'from_user', 'to_users', 'from_user_data']


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ['id', 'text', 'to_users', 'date']
