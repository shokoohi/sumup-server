from rest_framework import viewsets, status
from rest_framework.response import Response
from api.models import Country, Phone, Grade, Major, Orientation, Class, User, HashTag, Message, Image, Video, \
    Document, Like, Direct, Notification
from api.serializers import CountrySerializer, PhoneSerializer, GradeSerializer, MajorSerializer, \
    OrientationSerializer, ClassSerializer, UserSerializer, HashTagSerializer, MessageSerializer, ImageSerializer, \
    VideoSerializer, DocumentSerializer, LikeSerializer, DirectSerializer, NotificationSerializer


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class PhoneViewSet(viewsets.ModelViewSet):
    queryset = Phone.objects.all()
    serializer_class = PhoneSerializer


class GradeViewSet(viewsets.ModelViewSet):
    queryset = Grade.objects.all()
    serializer_class = GradeSerializer


class MajorViewSet(viewsets.ModelViewSet):
    queryset = Major.objects.all()
    serializer_class = MajorSerializer


class OrientationViewSet(viewsets.ModelViewSet):
    queryset = Orientation.objects.all()
    serializer_class = OrientationSerializer


class ClassViewSet(viewsets.ModelViewSet):
    queryset = Class.objects.all()
    serializer_class = ClassSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class HashTagViewSet(viewsets.ModelViewSet):
    queryset = HashTag.objects.all()
    serializer_class = HashTagSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    # Create new message with image, video or document
    def create(self, request, *args, **kwargs):
        message = None
        errors = list()  # Store all errors in a list
        message_serializer = MessageSerializer(data=request.data)
        if message_serializer.is_valid():
            message = message_serializer.save()
            data = request.data
            data._mutable = True
            data["message"] = str(message.id)
            # Check if any file is in request
            if 'image' in data:
                data['file'] = data['image']
                image_serializer = ImageSerializer(data=data)
                if image_serializer.is_valid():
                    image_serializer.save()
                else:
                    errors.append(image_serializer.errors)
            if 'video' in data:
                data['file'] = data['video']
                video_serializer = VideoSerializer(data=data)
                if video_serializer.is_valid():
                    video_serializer.save()
                else:
                    errors.append(video_serializer.errors)
            if 'document' in data:
                data['file'] = data['document']
                document_serializer = DocumentSerializer(data=data)
                if document_serializer.is_valid():
                    document_serializer.save()
                else:
                    errors.append(document_serializer.errors)
        else:
            errors.append(message_serializer.errors)
        # If we have any error
        if len(errors) > 0:
            return Response(data=errors, status=status.HTTP_400_BAD_REQUEST)
        # Anything is good!
        else:
            serializer = MessageSerializer(instance=message)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer


class VideoViewSet(viewsets.ModelViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer


class DocumentViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer


class LikeViewSet(viewsets.ModelViewSet):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer


class DirectViewSet(viewsets.ModelViewSet):
    queryset = Direct.objects.all()
    serializer_class = DirectSerializer


class NotificationViewSet(viewsets.ModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
