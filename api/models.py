from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from .utils import images_directory_path, videos_directory_path, documents_directory_path


class Country(models.Model):
    official_name = models.CharField(null=False, max_length=56)  # Longest country name have 56 characters!
    short_name = models.CharField(blank=True, max_length=3)
    numeric_code = models.IntegerField(
        null=False,
        default=364,
        validators=[
            MaxValueValidator(894),
            MinValueValidator(4)
        ]
    )
    phone_code = models.IntegerField(
        null=False,
        default=98,
        validators=[
            MaxValueValidator(999),
            MinValueValidator(1)
        ]
    )

    def __str__(self):
        return self.official_name


class Phone(models.Model):
    country = models.ForeignKey(to=Country, on_delete=models.CASCADE)
    activated = models.BooleanField(null=False, default=False)
    number = models.BigIntegerField(
        null=False,
        validators=[
            MaxValueValidator(9999999999),
            MinValueValidator(0)
        ]
    )

    def __str__(self):
        return str(self.number)


class Grade(models.Model):
    name = models.CharField(null=False, unique=True, max_length=20)
    minimum_level = models.IntegerField(
        null=False,
        validators=[
            MaxValueValidator(12),
            MinValueValidator(1)
        ]
    )
    maximum_level = models.IntegerField(
        null=False,
        validators=[
            MaxValueValidator(12),
            MinValueValidator(1)
        ]
    )

    def __str__(self):
        return self.name


class Major(models.Model):
    name = models.CharField(null=False, unique=True, max_length=32)

    def __str__(self):
        return self.name


class Orientation(models.Model):
    major = models.ForeignKey(to=Major, on_delete=models.CASCADE)
    name = models.CharField(null=False, unique=True, max_length=32)

    def __str__(self):
        return self.name


class Class(models.Model):
    grade = models.ForeignKey(to=Grade, on_delete=models.CASCADE)
    major = models.ForeignKey(to=Major, null=True, on_delete=models.CASCADE)
    orientation = models.ForeignKey(to=Orientation, null=True, on_delete=models.CASCADE)
    level = models.IntegerField(
        null=False,
        validators=[
            MaxValueValidator(12),
            MinValueValidator(1)
        ]
    )

    def __str__(self):
        return self.grade.name + ' ' + str(self.level)


class User(AbstractUser):
    online = models.BooleanField(null=False, default=False)
    last_seen = models.DateTimeField(null=False, auto_now=True)
    email = models.EmailField(null=False, max_length=254, unique=True)
    avatar = models.ImageField(null=True, default='uploads/avatars/default.png')
    country = models.ForeignKey(to=Country, on_delete=models.CASCADE)
    phone = models.OneToOneField(to=Phone, on_delete=models.CASCADE)
    class_level = models.ForeignKey(to=Class, on_delete=models.CASCADE)
    followers = models.ManyToManyField('self', related_name='followings', symmetrical=False)
    age = models.IntegerField(
        null=False,
        default=6,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(6)
        ]
    )

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class HashTag(models.Model):
    tag = models.CharField(null=False, max_length=16)

    def __str__(self):
        return self.tag


class Message(models.Model):
    text = models.CharField(null=False, max_length=280)
    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    reply_to = models.ForeignKey(to='self', related_name='comments', null=True, on_delete=models.CASCADE)
    hash_tags = models.ManyToManyField(to=HashTag)

    def __str__(self):
        return self.text


class Image(models.Model):
    file = models.ImageField(null=False, upload_to=images_directory_path)
    caption = models.CharField(null=True, max_length=64)
    message = models.ForeignKey(to=Message, related_name='images', on_delete=models.CASCADE)


class Video(models.Model):
    file = models.FileField(null=False, upload_to=videos_directory_path)
    caption = models.CharField(null=True, max_length=64)
    message = models.ForeignKey(to=Message, related_name='videos', on_delete=models.CASCADE)


class Document(models.Model):
    file = models.FileField(null=False, upload_to=documents_directory_path)
    caption = models.CharField(null=True, max_length=64)
    message = models.ForeignKey(to=Message, related_name='documents', on_delete=models.CASCADE)


class Like(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    message = models.ForeignKey(to=Message, related_name='likes', on_delete=models.CASCADE)


class Direct(models.Model):
    text = models.CharField(null=False, max_length=280)
    date = models.DateTimeField(auto_now=True)
    from_user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    to_users = models.ManyToManyField(to=User, related_name="conversation")

    def __str__(self):
        return self.text


class Notification(models.Model):
    text = models.CharField(null=False, max_length=280)
    to_users = models.ManyToManyField(to=User)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.text
